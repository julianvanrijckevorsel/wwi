<?php
include_once("php/connectdb.php");

$sql = "SELECT StockItemId, StockItemName, SupplierName, ColorName, U.PackageTypeName UPackageTypeName, O.PackageTypeName OPackageTypeName, 
Brand, Size, LeadTimeDays, QuantityPerOuter, IsChillerStock, Barcode, TaxRate, UnitPrice, RecommendedRetailPrice, TypicalWeightPerUnit, 
MarketingComments, Stock.InternalComments StockInternalComments, Photo, CustomFields, Tags, SearchDetails
    FROM stockitems Stock
    JOIN suppliers USING (SupplierID)
    LEFT JOIN colors USING (ColorId)
    JOIN PackageTypes U ON UnitPackageId = U.PackageTypeID
    JOIN PackageTypes O ON OuterPackageID = O.PackageTypeID;";
$statement = mysqli_prepare($connection, $sql);
mysqli_stmt_execute($statement);
$result = mysqli_stmt_get_result($statement);

print "<table>
        <th>ID</th><th>Productnaam</th>
        <th>Leverancier</th><th>Kleur</th><th>Als</th>
        <th>Verstuurd in</th><th>Merk</th>
        <th>Maat</th><th>LeadTimeDays</th>
        <th>Aantal per verzending</th><th>Verkoeld</th>
        <th>Barcode</th><th>Tax</th>
        <th>Prijs</th><th>Aanbevolen prijs</th>
        <th>Gewicht per stuk</th><th>MarketingComments</th>
        <th>InternalComments</th><th>Foto</th>
        <th>CustomFields</th><th>Tags</th>
        <th>Zoek details</th>";
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    print "<tr>
        <td>".$row["StockItemId"]."</td>
        <td>".$row["StockItemName"]."</td>
        <td>".$row["SupplierName"]."</td>
        <td>".$row["ColorName"]."</td>
        <td>".$row["UPackageTypeName"]."</td>
        <td>".$row["OPackageTypeName"]."</td>
        <td>".$row["Brand"]."</td>
        <td>".$row["Size"]."</td>
        <td>".$row["LeadTimeDays"]."</td>
        <td>".$row["QuantityPerOuter"]."</td>
        <td>".$row["IsChillerStock"]."</td>
        <td>".$row["StockItemId"]."</td>
        <td>".$row["Barcode"]."</td>
        <td>".$row["TaxRate"]."</td>
        <td>".$row["UnitPrice"]."</td>
        <td>".$row["RecommendedRetailPrice"]."</td>
        <td>".$row["TypicalWeightPerUnit"]."</td>
        <td>".$row["MarketingComments"]."</td>
        <td>".$row["StockInternalComments"]."</td>
        <td>".$row["Photo"]."</td>
        <td>".$row["CustomFields"]."</td>
        <td>".$row["Tags"]."</td>
        <td>".$row["SearchDetails"]."</td>   
</tr>";
}
print "</table>";

?>


<?php
mysqli_close($connection);
?>